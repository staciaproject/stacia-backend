from django.db import models

from stacia.apps.common.abstract_models import AbstractFile, WithDateCreated


class Portfolio(WithDateCreated):
    # date_created
    pass


class PortfolioImage(AbstractFile):
    # url

    portfolio = models.OneToOneField(
        Portfolio, related_name='image', on_delete=models.CASCADE)
