from django.contrib import admin

from stacia.apps.portfolio.models import Portfolio, PortfolioImage


class PortfolioImageInline(admin.TabularInline):
    model = PortfolioImage
    fields = ('url',)


@admin.register(Portfolio)
class PortfolioAdmin(admin.ModelAdmin):
    list_display = ('url',)

    def url(self, obj: Portfolio) -> str:
        if hasattr(obj, 'image'):
            return obj.image.url
        else:
            return ''

    inlines = (PortfolioImageInline,)
