from django.db import models

from stacia.apps.common.abstract_models import WithDateCreated
from stacia.apps.product.models import Product
from stacia.apps.user.models import User


class Review(WithDateCreated):
    # date_created

    user = models.ForeignKey(User, related_name='reviews', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, related_name='reviews', on_delete=models.CASCADE)
    rating = models.IntegerField()
    description = models.TextField()

    class Meta:
        unique_together = (('user', 'product'),)


class Like(WithDateCreated):
    # date_created

    user = models.ForeignKey(User, related_name='liked_products', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, related_name='likes', on_delete=models.CASCADE)

    class Meta:
        unique_together = (('user', 'product'),)
