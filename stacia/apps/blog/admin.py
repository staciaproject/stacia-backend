from django.contrib import admin

from stacia.apps.blog.models import Article, ArticleImage


class ArticleImageInline(admin.TabularInline):
    model = ArticleImage
    fields = ('url',)


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'text')

    inlines = (ArticleImageInline,)
