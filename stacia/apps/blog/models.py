from django.db import models

from stacia.apps.common.abstract_models import WithTimestamp, AbstractFile, WithDateCreated
from stacia.apps.user.models import User


class Article(WithTimestamp):
    # date_created
    # date_updated

    title = models.CharField(max_length=128)
    text = models.TextField()

    def __str__(self) -> str:
        return f'Article "{self.title}"'


class ArticleImage(AbstractFile):
    # url

    article = models.OneToOneField(Article, related_name='image', on_delete=models.CASCADE)


class ArticleLike(WithDateCreated):
    # date_created

    user = models.ForeignKey(User, related_name='liked_articles', on_delete=models.CASCADE)
    article = models.ForeignKey(Article, related_name='likes', on_delete=models.CASCADE)

    class Meta:
        unique_together = (('user', 'article'),)
