from django.contrib import admin

from stacia.apps.product.models import Product, ProductImage


class ProductImageInline(admin.TabularInline):
    model = ProductImage
    fields = ('url',)


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('sku', 'name', 'description', 'price', 'stock')

    inlines = (ProductImageInline,)
