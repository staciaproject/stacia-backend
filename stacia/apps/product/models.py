from django.db import models

from stacia.apps.common.abstract_models import WithTimestamp, AbstractFile


class Product(WithTimestamp):
    # date_created
    # date_updated

    sku = models.CharField(max_length=16, unique=True)
    name = models.CharField(max_length=64)
    description = models.TextField(blank=True, null=True, default="")
    how_to_use = models.TextField(blank=True, null=True, default="")
    price = models.PositiveIntegerField()
    stock = models.PositiveIntegerField()

    def __str__(self):
        return '({sku}) - {name}'.format(sku=self.sku, name=self.name)

    class Meta:
        ordering = ('-date_created',)


class ProductImage(AbstractFile):
    # url

    product = models.ForeignKey(
        Product, related_name='images', on_delete=models.CASCADE)
