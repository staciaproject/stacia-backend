from django.contrib.auth.models import AbstractUser, Group
from django.db import models
from django.db.transaction import atomic

from stacia.apps.common.abstract_models import AbstractAddress
from stacia.apps.user.managers import UserManager


class User(AbstractUser):
    email = models.EmailField(unique=True, null=True)
    phone_number = models.CharField(max_length=16, blank=True)
    birthday = models.DateField(null=True)
    is_subscribed = models.BooleanField(default=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = [field for field in AbstractUser.REQUIRED_FIELDS if field != 'email']

    ADMIN_GROUP_NAME = 'Admin'
    SUPER_ADMIN_GROUP_NAME = 'Super Admin'

    objects = UserManager()

    class Meta:
        app_label = 'user'

    @property
    def address(self) -> 'UserAddress':
        return self.addresses.filter(is_default=True).first()

    @property
    def is_admin(self) -> bool:
        return self.groups.filter(name=self.ADMIN_GROUP_NAME).exists()

    @property
    def is_super_admin(self) -> bool:
        return self.groups.filter(name=self.SUPER_ADMIN_GROUP_NAME).exists()

    def set_as_admin(self) -> None:
        admin_group = self._get_admin_group()
        self.groups.add(admin_group)

    def set_as_super_admin(self) -> None:
        admin_group = self._get_admin_group()
        super_admin_group = self._get_super_admin_group()

        self.groups.add(admin_group, super_admin_group)

    def unset_as_admin(self) -> None:
        admin_group = self._get_admin_group()
        super_admin_group = self._get_super_admin_group()

        self.groups.remove(admin_group, super_admin_group)

    def unset_as_super_admin(self) -> None:
        super_admin_group = self._get_super_admin_group()
        self.groups.remove(super_admin_group)

    def _get_admin_group(self) -> Group:
        return Group.objects.get_or_create(name=self.ADMIN_GROUP_NAME)

    def _get_super_admin_group(self) -> Group:
        return Group.objects.get_or_create(name=self.SUPER_ADMIN_GROUP_NAME)


class UserAddress(AbstractAddress):
    user = models.ForeignKey(User, related_name='addresses', on_delete=models.CASCADE)
    is_default = models.BooleanField(default=False)

    class Meta:
        app_label = 'user'

    def set_as_default(self) -> None:
        with atomic():
            self.user.addresses.all().update(is_default=False)
            self.is_default = True
            self.save()
