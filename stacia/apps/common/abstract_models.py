from django.db import models


class WithDateCreated(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class WithDateUpdated(models.Model):
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class WithTimestamp(WithDateCreated, WithDateUpdated):
    class Meta:
        abstract = True


class AbstractFile(models.Model):
    name = models.CharField(max_length=64, null=True, blank=True, default="")
    description = models.TextField(null=True, blank=True, default="")
    url = models.URLField()

    class Meta:
        abstract = True


class AbstractAddress(models.Model):
    street = models.CharField(max_length=256)
    city = models.CharField(max_length=32)
    province = models.CharField(max_length=32)
    country = models.CharField(max_length=32)
    postal_code = models.CharField(max_length=8)

    class Meta:
        abstract = True
