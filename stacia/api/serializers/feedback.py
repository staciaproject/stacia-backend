from rest_framework import serializers

from stacia.api.serializers.user import UserNameOnlySerializer
from stacia.apps.feedback.models import Review
from stacia.apps.product.models import Product
from stacia.apps.user.models import User


class ReviewSerializer(serializers.ModelSerializer):
    user = UserNameOnlySerializer()

    class Meta:
        model = Review
        fields = ('user', 'rating', 'description')


class ReviewCreationSerializer(serializers.ModelSerializer):
    user = serializers.SlugRelatedField(slug_field='username', queryset=User.objects.all())
    product = serializers.SlugRelatedField(slug_field='sku', queryset=Product.objects.all())

    class Meta:
        model = Review
        fields = ('user', 'product', 'rating', 'description')
