from rest_framework import serializers

from stacia.apps.blog.models import Article


class ArticleSerializer(serializers.ModelSerializer):
    def get_likes(self, obj: Article) -> int:
        return obj.likes.count()

    def get_is_liked(self, obj: Article) -> bool:
        user = self.context.get('user', None)

        if user is None:
            return False

        return obj.likes.filter(user=user).exists()

    class Meta:
        model = Article
        fields = ('id', 'date_created', 'date_updated', 'title', 'text', 'likes', 'is_liked')
        read_only_fields = ('id', 'date_created', 'date_updated')
