from django.db.models import Avg
from drf_writable_nested import WritableNestedModelSerializer
from rest_framework import serializers

from stacia.api.serializers.feedback import ReviewSerializer
from stacia.apps.product.models import Product, ProductImage


class ProductImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductImage
        fields = ('id', 'url')
        read_only_fields = ('id',)


class ProductSerializer(WritableNestedModelSerializer):
    images = ProductImageSerializer(many=True)
    reviews = ReviewSerializer(many=True)
    is_reviewed = serializers.SerializerMethodField()
    likes = serializers.SerializerMethodField()
    is_liked = serializers.SerializerMethodField()
    rating = serializers.SerializerMethodField()

    def get_is_reviewed(self, obj: Product) -> bool:
        user = self.context.get('user', None)

        if user is None:
            return False

        return obj.reviews.filter(user=user).exists()

    def get_likes(self, obj: Product) -> int:
        return obj.likes.count()

    def get_is_liked(self, obj: Product) -> bool:
        user = self.context.get('user', None)

        if user is None:
            return False

        return obj.likes.filter(user=user).exists()

    def get_rating(self, obj: Product) -> float:
        rating = obj.reviews.aggregate(average=Avg('rating'))['average']

        return rating if rating is not None else 0

    class Meta:
        model = Product
        fields = ('date_created', 'date_updated', 'sku', 'name',
                  'description', 'price', 'stock', 'images', 'reviews',
                  'is_reviewed', 'likes', 'is_liked', 'rating', 'how_to_use')
        read_only_fields = ('date_created', 'date_updated', 'reviews')
