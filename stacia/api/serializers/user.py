from typing import Dict

from django.db.transaction import atomic
from rest_framework import serializers

from stacia.api.serializers.common import AddressSerializer
from stacia.apps.user.models import User, UserAddress


class UserNameOnlySerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='first_name')

    class Meta:
        model = User
        fields = ('name',)


class UserAddressRegistrationSerializer(AddressSerializer):
    class Meta:
        model = UserAddress
        fields = AddressSerializer.Meta.fields + ('id',)
        extra_kwargs = {'id': {'read_only': True}}


class UserAddressWithoutUserSerializer(AddressSerializer):
    class Meta:
        model = UserAddress
        fields = AddressSerializer.Meta.fields + ('id', 'is_default')
        extra_kwargs = {'id': {'read_only': True}, 'is_default': {'read_only': True}}


class UserAddressSerializer(UserAddressWithoutUserSerializer):
    user = serializers.SlugRelatedField(slug_field='email', queryset=User.objects.all(), write_only=True)

    class Meta:
        model = UserAddress
        fields = UserAddressWithoutUserSerializer.Meta.fields + ('user',)
        extra_kwargs = {**UserAddressWithoutUserSerializer.Meta.extra_kwargs}


class RegistrationSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='first_name')
    birthday = serializers.DateField(format='%Y-%m-%d', input_formats=['%Y-%m-%d', 'iso-8601'])
    address = UserAddressRegistrationSerializer()

    class Meta:
        model = User
        fields = ('name', 'email', 'password', 'phone_number', 'birthday', 'address', 'is_subscribed')
        extra_kwargs = {'name': {'required': True},
                        'email': {'required': True},
                        'password': {'required': True, 'write_only': True},
                        'phone_number': {'required': True},
                        'birthday': {'required': True},
                        'address': {'required': True},
                        'is_subscribed': {'required': True}}

    def create(self, validated_data: Dict) -> User:
        address = validated_data.pop('address')

        with atomic():
            user = User.objects.create_user(**validated_data)
            UserAddress.objects.create(user=user, is_default=True, **address)

        return user


class UserSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='first_name')
    birthday = serializers.DateField(format='%Y-%m-%d', input_formats=['%Y-%m-%d', 'iso-8601'])
    addresses = UserAddressWithoutUserSerializer(many=True)

    class Meta:
        model = User
        fields = ('name', 'email', 'phone_number', 'birthday', 'addresses', 'is_subscribed')
