from rest_framework import serializers

from stacia.apps.common.abstract_models import AbstractAddress


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = AbstractAddress
        fields = ('street', 'city', 'province', 'country', 'postal_code')
        abstract = True
