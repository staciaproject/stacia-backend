from drf_writable_nested import WritableNestedModelSerializer
from rest_framework import serializers

from stacia.apps.portfolio.models import PortfolioImage, Portfolio


class ProductImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = PortfolioImage
        fields = ('id', 'url')
        read_only_fields = ('id',)


class PortfolioSerializer(WritableNestedModelSerializer):
    image = ProductImageSerializer()

    class Meta:
        model = Portfolio
        fields = ('id', 'date_created', 'image')
        read_only_fields = ('id', 'date_created')
