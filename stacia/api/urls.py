from django.urls import include, re_path
from rest_framework import routers
from rest_framework_simplejwt.views import TokenRefreshView, TokenObtainPairView

from stacia.api.views import product, feedback, user, portfolio, blog

router = routers.SimpleRouter()
router.register(r'products', product.ProductViewSet)
router.register(r'portfolios', portfolio.PortfolioViewSet)
router.register(r'article', blog.ArticleViewSet)
router.register(r'users/addresses', user.UserAddressViewSet)

urlpatterns = (
    re_path(r'^login/$',
            TokenObtainPairView.as_view(), name='login'),  # Deprecated
    re_path(r'^users/login/$',
            TokenObtainPairView.as_view(), name='user-login'),
    re_path(r'^users/register/$',
            user.RegistrationAPIView.as_view(), name='user-registration'),
    re_path(r'^users/me/$',
            user.MeAPIView.as_view(), name='user-me'),

    re_path(r'^token/refresh/$',
            TokenRefreshView.as_view(), name='refresh'),

    re_path(r'^products/(?P<sku>[^/]+)/reviews/$',
            feedback.ReviewCreationView.as_view(), name='product-review'),
    re_path(r'^products/(?P<sku>[^/]+)/likes/$',
            feedback.LikeView.as_view(), name='product-like'),  # Deprecated

    re_path(r'^', include(router.urls))
)
