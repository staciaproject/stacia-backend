from typing import Type

from django.db.models import QuerySet
from django.http import HttpResponse
from rest_framework.decorators import action
from rest_framework.generics import CreateAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from rest_framework.viewsets import ModelViewSet

from stacia.api.framework.permissions import IsUserOwner
from stacia.api.serializers.user import RegistrationSerializer, UserSerializer, UserAddressWithoutUserSerializer, \
    UserAddressSerializer
from stacia.apps.user.models import User, UserAddress


class RegistrationAPIView(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = RegistrationSerializer


class MeAPIView(RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self) -> User:
        return self.request.user


class UserAddressViewSet(ModelViewSet):
    queryset = UserAddress.objects.all()
    permission_classes = (IsAuthenticated, IsUserOwner)
    serializer_class = UserAddressWithoutUserSerializer

    def get_serializer_class(self) -> Type[Serializer]:
        if self.action == 'create':
            return UserAddressSerializer
        else:
            return UserAddressWithoutUserSerializer

    def get_queryset(self) -> QuerySet:
        return super().get_queryset().filter(user=self.request.user)

    def create(self, request: Request, *args, **kwargs) -> HttpResponse:
        request.data['user'] = request.user.email
        return super().create(request, *args, **kwargs)

    def destroy(self, request: Request, *args, **kwargs) -> HttpResponse:
        address: UserAddress = self.get_object()

        if address.is_default:
            return Response({'detail': 'Can not delete default address.'}, 400)

        return super().destroy(request, *args, **kwargs)

    @action(detail=True, methods=['post'], url_path='set-as-default')
    def set_as_default(self, request: Request, pk: int = None) -> HttpResponse:
        address: UserAddress = self.get_object()
        address.set_as_default()

        return Response(self.get_serializer(address).data)
