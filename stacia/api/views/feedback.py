from typing import Dict

from django.http import HttpResponse
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.serializers import BaseSerializer
from rest_framework.status import HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from stacia.api.serializers.feedback import ReviewCreationSerializer
from stacia.apps.feedback.models import Review, Like
from stacia.apps.product.models import Product


class ReviewCreationView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Review.objects.all()
    serializer_class = ReviewCreationSerializer

    def get_serializer(self, data: Dict) -> BaseSerializer:
        user = self.request.user
        sku = self.kwargs.get('sku')

        return self.get_serializer_class()(data={**data, 'user': user, 'product': sku})


class LikeView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request: Request, sku: str = None) -> HttpResponse:
        user = request.user

        try:
            product = Product.objects.get(sku=sku)
        except:
            return Response({'error': 'Product not found.'}, status=HTTP_400_BAD_REQUEST)

        is_liking = self.request.data.get('like', None)

        if is_liking is None:
            return Response({'error': 'Please enter "like" value.'}, status=HTTP_400_BAD_REQUEST)

        if is_liking:
            Like.objects.get_or_create(user=user, product=product)
        else:
            Like.objects.filter(user=user, product=product).delete()

        return Response()
