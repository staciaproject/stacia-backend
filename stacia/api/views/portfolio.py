from rest_framework.viewsets import ModelViewSet

from stacia.api.framework.permissions import IsAdminUserOrReadOnly
from stacia.api.serializers.portfolio import PortfolioSerializer
from stacia.apps.portfolio.models import Portfolio


class PortfolioViewSet(ModelViewSet):
    permission_classes = (IsAdminUserOrReadOnly,)
    queryset = Portfolio.objects.all()
    serializer_class = PortfolioSerializer
