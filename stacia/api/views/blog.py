from typing import Tuple, Dict

from django.http import HttpResponse
from rest_framework.decorators import action
from rest_framework.permissions import BasePermission, IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from stacia.api.framework.permissions import IsAdminUserOrReadOnly
from stacia.api.serializers.blog import ArticleSerializer
from stacia.apps.blog.models import Article, ArticleLike


class ArticleViewSet(ModelViewSet):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer

    def get_permissions(self) -> Tuple[BasePermission, ...]:
        if self.action in ['like', 'unlike']:
            return IsAuthenticated(),

        return IsAdminUserOrReadOnly(),

    def get_serializer_context(self) -> Dict:
        if self.request.user.is_authenticated:
            return {'user': self.request.user}

        return {}

    @action(detail=True, methods=['post'])
    def like(self, request: Request, *args, **kwargs) -> HttpResponse:
        user = request.user
        article: Article = self.get_object()

        is_already_liked = article.likes.filter(user=user).exists()

        if not is_already_liked:
            ArticleLike.objects.create(user=user, article=article)

        return Response(self.get_serializer(article).data)

    @action(detail=True, methods=['post'])
    def unlike(self, request: Request, *args, **kwargs) -> HttpResponse:
        user = request.user
        article: Article = self.get_object()

        ArticleLike.objects.filter(user=user, article=article).delete()

        return Response(self.get_serializer(article).data)
