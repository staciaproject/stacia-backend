from typing import Dict, Tuple

from django.http import HttpResponse
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated, BasePermission
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from stacia.api.framework.permissions import IsAdminUserOrReadOnly
from stacia.api.serializers.product import ProductSerializer
from stacia.apps.feedback.models import Like
from stacia.apps.product.models import Product


class ProductViewSet(ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    lookup_field = 'sku'

    def get_permissions(self) -> Tuple[BasePermission, ...]:
        if self.action in ['like', 'unlike']:
            return IsAuthenticated(),

        return IsAdminUserOrReadOnly(),

    def get_serializer_context(self) -> Dict:
        if self.request.user.is_authenticated:
            return {'user': self.request.user}

        return {}

    @action(detail=True, methods=['post'])
    def like(self, request: Request, *args, **kwargs) -> HttpResponse:
        user = request.user
        product: Product = self.get_object()

        is_already_liked = product.likes.filter(user=user).exists()

        if not is_already_liked:
            Like.objects.create(user=user, product=product)

        return Response(self.get_serializer(product).data)

    @action(detail=True, methods=['post'])
    def unlike(self, request: Request, *args, **kwargs) -> HttpResponse:
        user = request.user
        product: Product = self.get_object()

        Like.objects.filter(user=user, product=product).delete()

        return Response(self.get_serializer(product).data)
