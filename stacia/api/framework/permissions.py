from typing import Any

from django.views import View
from rest_framework.permissions import BasePermission, SAFE_METHODS
from rest_framework.request import Request


class IsAdminUserOrReadOnly(BasePermission):
    def has_permission(self, request: Request, view: View) -> bool:
        if request.method in SAFE_METHODS:
            return True

        if hasattr(request.user, 'is_admin'):
            return request.user.is_admin
        else:
            return False


class IsUserOwner(BasePermission):
    def has_object_permission(self, request: Request, view: View, obj: Any) -> bool:
        if not hasattr(obj, 'user'):
            raise Exception('Object has no owner')

        return request.user == obj.user
