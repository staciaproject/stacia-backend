### Getting Started

Lakukan langkah-langkah ini.

- Clone
-

```python
pip install pipenv
pipenv install
pipenv shell
python manage.py migrate
python manage.py createsuperuser
```

- Setiap mau jalanin, buka dulu `pipenv shell`, lalu `python manage.py runserver`

- Kalau mau nyoba login, kirim post ke http://localhost:8000/api/v1/login/, isinya json `{"username": username yang dibikin di atas, "password": password yang dibikin di atas}`
